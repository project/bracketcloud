<?php

/**
 * @file bracketcloud_handler_field_type.inc
 */

class bracketcloud_handler_field_type extends bracketcloud_handler_field {

  function render($values) {
    $value = $this->get_value($values);
    
    switch ($value) {
      case 'bracket':
        $name = t('Bracket');
        break;
        
      case 'round_robin':
        $name = t('Round robin');
        break;
        
      case 'ladder':
        $name = t('Ladder');
        break;
        
      case 'hybrid':
        $name = t('Hybrid');
        break;
    }
    
    return $name;
  }
}
