<?php

/**
 * @file bracketcloud_handler_filter_tid.inc
 */

class bracketcloud_handler_filter_tid extends bracketcloud_handler_filter_equality {

  function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Tournament ID'),
      '#default_value' => $this->value,
      '#description' => 'Separate multiple tournament IDs by a comma delimiter.',
    );
  }

}
