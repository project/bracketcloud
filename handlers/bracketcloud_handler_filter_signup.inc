<?php

/**
 * @file bracketcloud_handler_filter_signup.inc
 */

class bracketcloud_handler_filter_signup extends bracketcloud_handler_filter_equality {

  function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#type' => 'select',
      '#title' => t('Tournament signup status'),
      '#default_value' => $this->value,
      '#options' => array(
        'false' => t('Disabled'),
        'true' => t('Enabled'),
      ),
      '#description' => t('Signup status determines if BracketCloud users can sign up to this tournament.')
    );
  }
  
}
