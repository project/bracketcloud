<?php

/**
 * @file bracketcloud_handler_field_date.inc
 */

class bracketcloud_handler_field_date extends views_handler_field_date {
  
  /**
   * Called to add the field to a query.
   */
  function query() {
    $this->field_alias = $this->real_field;
  }
}
