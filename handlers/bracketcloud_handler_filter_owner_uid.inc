<?php

/**
 * @file bracketcloud_handler_filter_owner.inc
 */

class bracketcloud_handler_filter_owner_uid extends bracketcloud_handler_filter_equality {

  function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('BracketCloud User ID'),
      '#default_value' => $this->value,
      '#description' => t('Filters tournaments by those that this BracketCloud user is an owner of.'),
    );
  }
  
}
