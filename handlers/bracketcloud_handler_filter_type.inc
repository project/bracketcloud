<?php

/**
 * @file bracketcloud_handler_filter_type.inc
 */

class bracketcloud_handler_filter_type extends bracketcloud_handler_filter_equality {

  function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#type' => 'select',
      '#title' => t('Tournament type'),
      '#options' => array(
        'bracket' => t('Bracket'),
        'round_robin' => t('Round robin'),
        'ladder' => t('Ladder'),
        'hybrid' => t('Hybrid'),
      ),
      '#default_value' => $this->value,
    );
  }

}
