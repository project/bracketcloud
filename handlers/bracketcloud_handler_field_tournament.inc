<?php

/**
 * @file bracketcloud_views_handler_field_tournament.inc
 */

/**
 * Field handler to provide simple renderer that allows linking to a tournament.
 */
class bracketcloud_handler_field_tournament extends views_handler_field {

  function init(&$view, &$options) {
    parent::init($view, $options);

    if (!empty($this->options['link_to_bracketcloud'])) {
      $this->aliases['tid'] = 'tid';
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_bracketcloud'] = array('default' => TRUE);
    return $options;
  }

  /**
   * Provide link.
   */
  function options_form(&$form, &$form_state) {
    $form['link_to_bracketcloud'] = array(
      '#title' => t('Link this field to the BracketCloud tournament page.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_bracketcloud']),
    );
    parent::options_form($form, $form_state);
  }

  /**
   * Render whatever the data is as a link to the BracketCloud page.
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    if (!empty($this->options['link_to_bracketcloud']) && $data !== NULL && $data !== '') {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "http://bracketcloud.com/tournament/" . $this->get_value($values, 'tid');
    }
    return $data;
  }

  function render($values) {
    $value = $this->get_value($values);
    return $this->render_link($this->sanitize_value($value), $values);
  }

  /**
   * Called to add the field to a query.
   */
  function query() {
    $this->field_alias = $this->real_field;
  }
}
