<?php

/**
 * @file bracketcloud_handler_field_picture.inc
 */

class bracketcloud_handler_field_picture extends views_handler_field {
  
  function option_definition() {
    $options = parent::option_definition();

    $options['picture_type'] = array('default' => 'small');

    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['picture_type'] = array(
      '#type' => 'select',
      '#title' => t('Tournament picture size'),
      '#options' => array(
        'small' => t('Small') . ' (100px * 100px)',
        'medium' => t('Medium') . ' (200px * 200px)',
        'original' => t('Original'),
      ),
      '#default_value' => $this->options['picture_type'],
      '#required' => TRUE,
    );
  }

  function render($values) {
    $url = $this->get_value($values);
    return theme('image', array('path' => $url));
  }

  /**
   * Called to add the field to a query.
   */
  function query() {
    $this->field_alias = $this->real_field . '_' . $this->options['picture_type'];;
  }
}
