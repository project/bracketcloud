<?php

/**
 * @file bracketcloud_handler_sort.inc
 */

class bracketcloud_handler_sort extends views_handler_sort {

  function query() {
    $sort = $this->real_field . '-' . $this->options['order'];
    
    if (isset($this->query->request_arguments['sort'])) {
      $this->query->request_arguments['sort'] .= ',' . $sort;
    }
    else {
      $this->query->request_arguments['sort'] = $sort;
    }
  }
}
