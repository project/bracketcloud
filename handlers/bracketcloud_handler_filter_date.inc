<?php

/**
 * @file bracketcloud_handler_filter_date.inc
 */

class bracketcloud_handler_filter_date extends views_handler_filter_date {
  
  function operators() {
    return array(
      '=' => array(
        'title' => t('Is equal to'),
        'method' => 'op_simple',
        'short' => t('='),
        'values' => 1,
      ),    
      '<' => array(
        'title' => t('Is less than'),
        'method' => 'op_simple',
        'short' => t('<'),
        'values' => 1,
      ),
      '>=' => array(
        'title' => t('Is greater than or equal to'),
        'method' => 'op_simple',
        'short' => t('>='),
        'values' => 1,
      ),      
      'between' => array(
        'title' => t('Is between'),
        'method' => 'op_between',
        'short' => t('between'),
        'values' => 2,
      ),      
    );    
  }
  
  /**
   * Provide a list of all the operators
   */
  function operator_options($which = 'title') {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  function operator_values($values = 1) {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      if ($info['values'] == $values) {
        $options[] = $id;
      }
    }

    return $options;
  }  
  
  function op_between($field) {
    $since = intval(strtotime($this->value['min'], REQUEST_TIME));
    $until = intval(strtotime($this->value['max'], REQUEST_TIME));
    $this->query->request_arguments['since'] = $since;
    $this->query->request_arguments['until'] = $until;
  }

  function op_simple($field) {
    $value = intval(strtotime($this->value['value'], REQUEST_TIME));
    
    switch ($this->operator) {
      case '<':
        $key = 'until';
        break;
        
      case '>=':
        $key = 'since';
        break;
        
      case '=':
        $key = $this->real_field;
        break;
    }

    $this->query->request_arguments[$key] = $value;
  }
}
