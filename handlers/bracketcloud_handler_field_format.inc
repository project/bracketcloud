<?php

/**
 * @file bracketcloud_handler_field_format.inc
 */

class bracketcloud_handler_field_format extends bracketcloud_handler_field {

  function render($values) {
    $value = $this->get_value($values);
    
    if (isset($values->type)) {
      switch ($values->type) {
        case 'bracket':
          return $value === 1 ? t('Single Elimination') : t('Double Elimination'); 
      }
    }
    
    return $this->sanitize_value($value);
  }
}
