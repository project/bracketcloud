<?php

/**
 * @file bracketcloud_views_plugin_query.inc
 */

/**
 * Class for implementing a simple Views query backend plugin, that uses our custom class
 * to execute requests to get information about BracketCloud tournaments.
 *
 * TODO Possible improvement: implementing pager functionality.
 * See the EntityFieldQuery Views Backend module for an example:
 * http://drupalcode.org/project/efq_views.git/blob/refs/heads/7.x-1.x:/efq_views_plugin_query.inc
 */
class bracketcloud_views_plugin_query extends views_plugin_query {

  /**
   * The BracketCloudAPIRequest object.
   */
  var $request;

  /**
   * Constructor; Create the basic request object.
   */
  function init($base_table, $base_field, $options) {
    parent::init($base_table, $base_field, $options);
    $api_key = variable_get('bracketcloud_api_key');
    
    if (empty($api_key)) {
      drupal_set_message(t('You must enter a BracketCloud API Key from the <a href="!url">configuration page</a> first.', array('!url' => url('admin/config/services/bracketcloud'))), 'error');
      return FALSE;
    }
    
    $this->request = new BracketCloudAPIRequest($api_key);
  }

  /**
   * Builds the necessary info to execute the query.
   */
  function build(&$view) {
    $view->build_info['bracketcloud_request'] = $this->request;

    // Adding arguments to the request.
    if (isset($view->query->request_arguments)) {
      foreach ($view->query->request_arguments as $arg_key => $arg_value) {
        $view->build_info['bracketcloud_request']->addArgument($arg_key, $arg_value);
      }
    }
    
    $view->init_pager();
    if ($this->pager->plugin_name == 'some' && !empty($this->pager->options['items_per_page'])) {
      $view->build_info['bracketcloud_request']->addArgument('limit', $this->pager->options['items_per_page']);
    }
  }

  /**
   * Executes the request and fills the associated view object with according
   * values.
   */
  function execute(&$view) {
    $request = $view->build_info['bracketcloud_request'];

    $start = microtime(true);

    $view->result = array();
    $result = $request->execute();
    
    if (isset($result->error)) {
      drupal_set_message(t('BracketCloud API returned an error: !error', array('!error' => $result->error)), 'error');
    }
    else {
      $view->result = $result;
    }

    $view->execute_time = microtime(true) - $start;
  }
}