<?php

/**
 * @file bracketcloud.views.inc
 */

/**
 * Implements hook_views_data().
 */
function bracketcloud_views_data() {
  $data = array();

  // Base table definition.
  $data['bracketcloud']['table'] = array(
    'group' => t('BracketCloud'),
    'base' => array(
      'title' => t('BracketCloud tournaments'),
      'help' => t('Information about BracketCloud tournaments fetched through the BracketCloud REST API.'),
      'query class' => 'bracketcloud_query',
    ),
  );

  // Tournament ID
  $data['bracketcloud']['tid'] = array(
    'title' => t('Tournament ID'),
    'help' => t('Unique identifier of the tournament.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_tournament',
    ),
    'sort' => array(
      'handler' => 'bracketcloud_handler_sort',
    ),
    'filter' => array(
      'handler' => 'bracketcloud_handler_filter_tid',
    ),
  );
  
  // Title
  $data['bracketcloud']['title'] = array(
    'title' => t('Title'),
    'help' => t('The tournament title.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_tournament',
    ),
    'sort' => array(
      'handler' => 'bracketcloud_handler_sort',
    ),
  );
  
  // Picture
  $data['bracketcloud']['picture'] = array(
    'title' => t('Picture'),
    'help' => t('The tournament picture.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_picture',
    ),
  );   
  
  // Participant
  $data['bracketcloud']['participant'] = array(
    'title' => t('Participant'),
    'help' => t('Tournament participant BracketCloud user ID.'),
    'filter' => array(
      'handler' => 'bracketcloud_handler_filter_participant',
    ),
  );
  
  // Owner
  $data['bracketcloud']['owner'] = array(
    'title' => t('Owner'),
    'help' => t('Tournament owner BracketCloud user name.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field',
    ),
  );
  
  // Owner UID
  $data['bracketcloud']['owner_uid'] = array(
    'title' => t('Owner user ID'),
    'help' => t('Tournament owner BracketCloud user ID.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field',
    ),
    'filter' => array(
      'handler' => 'bracketcloud_handler_filter_owner_uid',
    ),
  );  
  
  // Signup
  $data['bracketcloud']['signup'] = array(
    'title' => t('Signup'),
    'help' => t('Tournament signup status.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_boolean',
    ),
    'filter' => array(
      'handler' => 'bracketcloud_handler_filter_signup',
    ),
  );
  
  // Created
  $data['bracketcloud']['created'] = array(
    'title' => t('Created'),
    'help' => t('The date that the tournament was created.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_date',
    ),
    'filter' => array(
      'handler' => 'bracketcloud_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'bracketcloud_handler_sort',
    ),    
  );
  
  // Created
  $data['bracketcloud']['changed'] = array(
    'title' => t('Changed'),
    'help' => t('The date that the tournament was last updated.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_date',
    ),
    'sort' => array(
      'handler' => 'bracketcloud_handler_sort',
    ),
  );
  
  // Deadline
  $data['bracketcloud']['deadline'] = array(
    'title' => t('Signup deadline'),
    'help' => t('The tournament signup deadline date.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_date',
    ),
    'sort' => array(
      'handler' => 'bracketcloud_handler_sort',
    ),
  );
  
  // Score entry
  $data['bracketcloud']['score_entry'] = array(
    'title' => t('Score entry'),
    'help' => t('Participant score entry permission status.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_boolean',
    ),
  );
  
  // Size
  $data['bracketcloud']['size'] = array(
    'title' => t('Size'),
    'help' => t('The tournament size.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field',
    ),
  );
    
  // Type
  $data['bracketcloud']['type'] = array(
    'title' => t('Type'),
    'help' => t('The tournament type.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_type',
    ),
    'filter' => array(
      'handler' => 'bracketcloud_handler_filter_type',
    ),
  );
  
  // Format
  $data['bracketcloud']['format'] = array(
    'title' => t('Format'),
    'help' => t('The tournaments format. Value changes depending on tournament type.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_format',
    ),
  );
  
  // Bronze match
  $data['bracketcloud']['bronze_match'] = array(
    'title' => t('Bronze match'),
    'help' => t('Brackets only. Bronze place match status.'),
    'field' => array(
      'handler' => 'bracketcloud_handler_field_boolean',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_plugins().
 */
function bracketcloud_views_plugins() {
  $plugins = array(
    'query' => array(
      'bracketcloud_query' => array(
        'title' => t('BracketCloudAPIRequest'),
        'help' => t('Uses BracketCloudAPIRequest for querying information about BracketCloud tournaments fetched through the BracketCloud REST API.'),
        'handler' => 'bracketcloud_views_plugin_query',
      ),
    ),
  );

  return $plugins;
}
