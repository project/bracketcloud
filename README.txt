-- SUMMARY --

This module provides intergration with the BracketCloud API. 

For a full description of the module, visit the project page:
  http://drupal.org/project/bracketcloud

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/bracketcloud

-- REQUIREMENTS --

* Read the BracketCloud API terms of use:
  http://bracketcloud.com/legal/api-terms


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Customize the module settings in Administration » Configuration

* Make sure that you set an API Key.


-- CONTACT --

Current maintainers:
* Joe Fender (drupaljoe) - http://drupal.org/user/613292

This project has been sponsored by:
* BracketCloud
  Tournament management in the Cloud. Visit http://bracketcloud.com for more information.
