<?php

/**
 * @file bracketcloud.admin.inc
 */

/**
 * Form builder; BracketCloud API administration form.
 */
function bracketcloud_admin_form($form) {
  $form = array();
  
  $form['bracketcloud_api_key'] = array(
    '#title' => t('API key'),
    '#description' => t('Enter your BracketCloud API Key which can be found at your <a href="!account">BracketCloud account settings page</a>.', array('!account' => url('http://bracketcloud.com/user/settings', array('external' => TRUE)))),
    '#type' => 'textfield',
    '#default_value' => variable_get('bracketcloud_api_key', ''),
    '#required' => TRUE,
  );
  
  return system_settings_form($form);
}
